import KratosMultiphysics as km
import KratosMultiphysics.StructuralMechanicsApplication as sm
from KratosMultiphysics.StructuralMechanicsApplication.structural_mechanics_analysis import StructuralMechanicsAnalysis
import numpy as np
import datetime
import os

# Set up session to find Kratos
os.system("source /opt/intel/mkl/bin/mklvars.sh intel64 lp64")
SimulationStart = '{:%d.%m.%Y %H:%M:%S}'.format(datetime.datetime.now())

# Calculation options
Eval = 1
SensAn = 0
Opt = 0

#change node numbers from giD
MassNodes = np.array([20464])
nMass = len(MassNodes)
x0 = np.ones([nMass, ])*1e-9


class StructuralMechanicsAnalysisWithEigenPostProcessing(StructuralMechanicsAnalysis):
    #def Finalize(self):
    #    super(StructuralMechanicsAnalysisWithEigenPostProcessing, self).Finalize()
    #    main_model_part_name = self.project_parameters["solver_settings"]["model_part_name"].GetString()
    #    main_model_part = self.model[main_model_part_name]
    #    self.Lambda = main_model_part.ProcessInfo[sm.EIGENVALUE_VECTOR]
    #    mass_process = sm.TotalStructuralMassProcess(main_model_part)
    #    mass_process.Execute()
    #    self.mass = main_model_part.ProcessInfo[km.NODAL_MASS]
    pass

def SysEval(m):
    with open("ProjectParameters.json", 'r') as parameter_file:
        parameters = km.Parameters(parameter_file.read())
    model = km.Model()
    simulation = StructuralMechanicsAnalysisWithEigenPostProcessing(model, parameters)
    simulation.Initialize()
    solver = simulation._GetSolver()
    computing_model_part = solver.GetComputingModelPart()
    stiffness = 0.0
    damping = 0.0
    for ii in range(len(MassNodes)):
        element = computing_model_part.CreateNewElement("NodalConcentratedElement3D1N", 999999+ii, [MassNodes[ii]], None)
        element.SetValue(km.NODAL_MASS, m[ii])
        element.SetValue(sm.NODAL_DISPLACEMENT_STIFFNESS, [0, stiffness, 0])
        element.SetValue(sm.NODAL_DAMPING_RATIO, [0, damping, 0])
    simulation.RunSolutionLoop()
    simulation.Finalize()
    TotalMass = simulation.mass
    Lambda = simulation.Lambda
    omega = np.sqrt(Lambda)
    frequency = omega/(2*np.pi)
    return(frequency, TotalMass)

#here vector of masses !!! change it
x0 = np.array([117e-6])


if Eval:
    frequency0, m0 = SysEval(x0)
    print('Eigenfrequencies [Hz]:\n' + str(frequency0.tolist()))
    print('Totoal mass m [kg]:\n' + str(m0))

    SimulationFinish = '{:%d.%m.%Y %H:%M:%S}'.format(datetime.datetime.now())
    print('Simulation start : ' + SimulationStart)
    print('Simulation finish: ' + SimulationFinish)

